# haskell-spock-playground

Here I'm learning Haskell web development. 

## Notes

I chose Spock web framework. There seems to be a lot of other options and since I am new to this ecosystem I can't really say why I chose this one. I did a little bit of research and it seems like a good starting point. It seems to provide enough type safety without being too aesotheric for me to understand at my level (like Yesod).

I've started with this tutorial: https://www.spock.li/tutorials/getting-started

At the time of writing not all required dependenices were mentioned. See this issue: https://github.com/agrafix/Spock/issues/162

## Setup

Best way to install Haskell seems to be Stack: https://docs.haskellstack.org/en/stable/README/

To setup editor integration (Neovim) I use Coc with haskell-ide-engine (https://github.com/haskell/haskell-ide-engine#installation). Installation is a little tricky and takes a long time. Basically I cloned the git repository (with submodules) and built it with stack:

```sh
git clone https://github.com/haskell/haskell-ide-engine --recurse-submodules
cd haskell-ide-engine
stack ./install.hs build
```

This creates `hie` and `hie-wrapper` executables and places them in `${HOME}/.local/bin` - don't forget to add this directory to your path. See https://github.com/tad-lispy/dotfiles/commit/5c546b0217e6e64fc1cc02f30948d7378232828c for related changes in my dotfiles.

Other tools can be easily installed using stack:

```sh
stack install brittany  # code formatting
stack install hlint
stack install ghcid     # watching and testing / restarting - see ./Makefile
```

