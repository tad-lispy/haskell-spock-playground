{-# LANGUAGE OverloadedStrings #-}

module Main where

import           Web.Spock
import           Web.Spock.Config
import           Control.Monad.Trans
import           Data.IORef
import           Data.Monoid
import qualified Data.Text                     as Text

data Session =
  EmptySession

data AppState =
  AppState (IORef Int)

main :: IO ()
main = do
  ref         <- newIORef 0
  spockConfig <- defaultSpockCfg EmptySession PCNoDatabase (AppState ref)
  runSpock 8080 (spock spockConfig app)

app :: SpockM () Session AppState ()
app = do
  get root $ text "Hello, Spock!"
  get ("hello" <//> var) $ \name -> do
    (AppState ref) <- getState
    visitorNumber  <- liftIO $ atomicModifyIORef' ref $ \i -> (i + 1, i + 1)
    text
      (  "Hello "
      <> name
      <> ", you are a visitor # "
      <> Text.pack (show visitorNumber)
      <> ". Isn't it splendid?"
      )


